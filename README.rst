cdproject-scala
==============

This projects offer command line fuzzy completing and a short-cut to project
directories.

Installation
-------

If for some reason you want to try it out, you can do this:::

   git clone git@gitlab.com/janilcgarcia/cdproject-scala.git
   # or using https
   git clone https://gitlab.com/janilcgarcia/cdproject-scala.git

   # download and install sbt (https://www.scala-sbt.org/download.html)

   cd cdproejct-scala
   sbt nativeImage

   cp target/native-image/cdproject-scala $ANYWHERE_IN_YOUR_PATH

   # and then source cdp.bash into your environment, anyway you want
   echo "source \"$PWD/cdp.bash\"" >> ~/.bashrc

Then it's ready to use. Just call ``cdp`` in your shell.

Configuration
-----

The program can be configured by editing the ``config.yaml`` in its XDG config
dir, normally ``~/.config/cdproject/config.yaml``. The config has three keys:

project-file-patterns
  File name patterns used to determine if a directory is a project. If the
  directory contains any file that matches any of those patterns, it is
  considered a directory. This configuration is useful when calling ``cdp`` with
  the ``-p`` or when the basic directory search is exhausted. Patterns starting
  with ``ext/`` matches extensions separated by commas (,). Patterns starting
  with ``re/`` matches regular expressions.

project-directories
  Root directories where the projects are found. Must be existing directories
  in your system.

default-root
  One of the previously named roots. Used when no root is defined in the query
  (the query does not contain a :).
