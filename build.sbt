
ThisBuild / scalacOptions += "-deprecation"

lazy val root = project
  .in(file("."))
  .settings(
    name := "cdproject-scala",
    version := "0.1.0",

    scalaVersion := "2.13.6",

    libraryDependencies ++= Seq(
      "org.snakeyaml" % "snakeyaml-engine" % "2.3",
      "com.github.scopt" %% "scopt" % "4.0.1",
      "org.scala-lang.modules" %% "scala-parser-combinators" % "2.0.0",

      "org.scalatest" %% "scalatest" % "3.2.9" % Test
    ),

    Compile / mainClass := Some("cdproject.Main"),

    nativeImageOptions += "-H:IncludeResources=.*\\.yaml$"
  )
  .enablePlugins(NativeImagePlugin)
