
cdp() {
    result=$(cdproject-scala "$@")

    if [ $? = 0 ]; then
        cd $result
    fi
}

complete -C cdproject-scala cdp

