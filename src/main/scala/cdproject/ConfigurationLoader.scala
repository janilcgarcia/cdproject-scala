package cdproject

import java.io.File
import scala.util.Success
import scala.util.Failure
import java.io.FileNotFoundException
import scala.util.Try
import java.io.FileOutputStream
import scala.util.Using

object FileExtensions {
  def file(name: String): File = new File(name)

  object Implicits {
    implicit class WrappedFileOps(override val file: File) extends FileOps
    def fileFromWrappFileOpsConversion(file: WrappedFileOps): File = file.file
  }
}

trait FileOps {
  protected def file: File

  def /(name: String): File =
    if (file.toString.isEmpty)
      new File(name)
    else
      new File(file, name)

  def /(other: File): File =
    this / other.toString()
}

class ConfigurationLoader private () {
  import FileExtensions.file
  import FileExtensions.Implicits._

  private val configFileNames = Seq("config.yaml", "config.yml")

  private def getConfigDir: Try[File] = Try {
    Option(System.getenv("XDG_CONFIG_HOME"))
      .map(home => file(home))
      .orElse(
        Option(System.getenv("HOME")).map(file(_) / ".config")
      )
      .orElse(
        Option(System.getProperty("user.home")).map(file(_) / ".config")
      )
  } flatMap {
    case Some(home) => Success(home)
    case None => Failure(new FileNotFoundException("Can't find the user home "
        + "or it's config directory"))
  }

  private def createConfigFile(output: File): Try[File] = Using.Manager { m =>
    val config = m(
      Option(getClass().getResourceAsStream("config.yaml"))
        .getOrElse(
          getClass().getClassLoader().getResourceAsStream("config.yaml")))

    val os = m(new FileOutputStream(output))

    config.transferTo(os)
    output
  }

  private def getConfigFile(baseConfig: File): Try[File] = {
    val configDir = baseConfig / "cdproject"
    val configFile = configDir / "config.yaml"

    if (!configDir.exists()) {
      Try {
        configDir.mkdirs()
      }.flatMap(_ => createConfigFile(configFile))
    } else if (!configDir.isDirectory()) {
      Failure(new FileNotFoundException(s"$configDir is supposed to be "
        + "directory, can't load configuration file"))
    } else {
      configFileNames.map(configDir / _)
        .dropWhile(!_.isFile()).headOption match {
          case Some(c) => Success(c)
          case None => createConfigFile(configFile)
        }
    }
  }

  def loadConfig(): Try[Config] = for {
    dir <- getConfigDir
    file <- getConfigFile(dir)
    config <- Config.loadFromFile(file.getAbsolutePath().toString())
  } yield config
}

object ConfigurationLoader {
  def apply(): ConfigurationLoader = new ConfigurationLoader
}
