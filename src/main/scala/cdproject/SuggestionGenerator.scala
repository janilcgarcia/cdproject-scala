package cdproject

import java.io.FileFilter
import java.io.File
import java.nio.file.Path

class SuggestionGenerator(val config: Config) {
  import FileExtensions.file
  import FileExtensions.Implicits._
  import CodePoint._

  private val options = Seq("-p", "--projects", "-h", "--help")

  private def directories(root: String, parent: String): Seq[String] = {
    val d = config.getDirectory(root) / parent

    d.list().toSeq
  }

  private def matchesAnyPattern(file: File): Boolean =
    config.projectFilePatterns.exists { pattern =>
      pattern.matches(file.getName()) ||
        (file.isDirectory() && pattern.matches(file.getName() + "/"))
    }


  private def hasProjectFile(file: File): Boolean = {
    val children = file.listFiles().toIndexedSeq

    children.exists(matchesAnyPattern(_))
  }

  private def isDirectory(file: File): Boolean =
    file.isDirectory() && !file.getName().startsWith(".")

  private def projectDirectories(projectRoot: String): Seq[String] = {
    val root = config.getDirectory(projectRoot)
    
    val ordering = Ordering.ordered[Path] { a => b =>
      val c = a.getNameCount().compareTo(b.getNameCount())

      if (c == 0)
        a.compareTo(b)
      else
        c
    }

    collectProjects(Seq.empty, root).map(p => root.toPath().relativize(p.toPath()))
      .toSeq.sorted(ordering).map(_.toString)
  }

  private def collectProjects(acc: Seq[File], root: File): Seq[File] =
    root.listFiles(isDirectory _).foldLeft(acc) { (acc, d) =>
      if (hasProjectFile(d))
        d +: acc
      else
        collectProjects(acc, d)
    }

  private def filteredDirectories(root: String, query: String): Seq[String] = {
    val p = FileExtensions.file(query)

    val (parent, baseName) = if (p.getParent != null)
      (p.getParentFile(), p.getName())
    else
      (file(""), p)

    val result = directories(root, parent.toString)

    if (query.isEmpty())
      result
    else
      Myers.bestMatches[Int, Seq[Int]](
        baseName.toString.codePointSeq,
        result.map(_.codePointSeq)
      ).filter { case (_, score) =>
          score >= config.threshold
      }.map { case (cs, _) =>
          val s = new String(cs.toArray, 0, cs.size)
          (parent / s).toString
      }
  }

  private def filteredProjects(root: String, query: String): Seq[String] = {
    val result = projectDirectories(root)

    if (query.isEmpty())
      result
    else
      Myers.bestMatches[Int, Seq[Int]](
        query.codePointSeq,
        result.map(_.codePointSeq)
      ).filter { case (_, score) =>
          score >= config.threshold
      }.map { case (cs, _) =>
          new String(cs.toArray, 0, cs.size)
      }
  }

  def optionSuggestions(prefix: String): Seq[String] =
    options.filter(_.startsWith(prefix))


  def directorySuggestions(root: String, query: String, projects: Boolean)
      : Seq[String] = {
    if (projects) {
      filteredProjects(root, query)
    } else {
      val simple = filteredDirectories(root, query)

      if (simple.isEmpty)
        filteredProjects(root, query)
      else
        simple
    }
  }
}

object SuggestionGenerator {
  def apply(config: Config): SuggestionGenerator =
    new SuggestionGenerator(config)

  def apply(): SuggestionGenerator =
    SuggestionGenerator(ConfigurationLoader().loadConfig().get)
}
