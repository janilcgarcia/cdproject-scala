package cdproject

import scala.io.StdIn
import java.nio.file.Path
import scala.util.matching.Regex
import scala.util.{Either, Right, Left}

import org.snakeyaml.engine.v2.api._
import scala.util.Try
import scala.util.Using
import java.io.FileReader
import scala.util.Success
import scala.util.Failure
import cdproject.yaml._
import scala.annotation.tailrec
import java.io.File
import java.util.Comparator

import scopt.OParser
import java.util.stream.Collectors
import java.io.FileNotFoundException
import java.io.FileOutputStream

object Main {
  import FileExtensions.Implicits._

  private case class Options(
    val isProject: Boolean,
    val root: String,
    val query: String
  ) {
    def setQuery(q: String): Options = {
      val (root, query) = splitQuery(q)

      Options(isProject, root, query)
    }
  }

  private def onGenerate(config: Config, opts: Options): Unit = {
    val root = config.getDirectory(opts.root)

    println((root / opts.query).toString)
  }

  private def splitQuery(q: String): (String, String) = {
    val sep = q.indexOf(':')

    if (sep < 0)
      "" -> q
    else
      q.substring(0, sep) -> q.substring(sep + 1)
  }
      
  private def onCompleting(
    config: Config,
    line: String,
    position: Int
  ): Unit = {
    val suggestions = SuggestionGenerator(config)

    ShCommandParser.split(line) match {
      case Right(captures) =>
        val word = BashCompletingFunctions.completingWord(
          line, captures, position)

        if (word.startsWith("-")) {
          suggestions.optionSuggestions(word).foreach(println)
        } else {
          val isProject = (captures.contains("-p") ||
            captures.contains("--projects"))

          val separator = word.indexOf(':')

          val (root, query) = splitQuery(word)

          suggestions.directorySuggestions(root, query, isProject)
            .foreach(println)
        }

      case Left(_) =>
    }
  }

  def main(args: Array[String]): Unit = {
    val builder = OParser.builder[Options]

    val parser = {
      import builder._

      OParser.sequence(
        programName("cdproject"),
        head("cdproject", "0.1"),

        opt[Unit]('p', "projects")
          .action((_, c) => c.copy(isProject = true))
          .text("force search by project (it will recursevely look up "
            + "projects on the configured extensions)"),

        arg[String]("<query>")
          .optional()
          .validate { q =>
            val pos = q.indexOf(':')

            if (pos > 0 && q.indexOf(':', pos + 1) > 0)
              Left("Invalid pattern, can only contain one ':'")
            else
              Right(())
          }
          .action { case (q, c) => c.setQuery(q) }
          .text(
            "pattern to lookup in the project directory. It takes the form "
              + "root:pattern where root is one of the roots defined at the "
              + "config file. The pattern will be approximately matched to the "
              + "projects."
          ),

        help('h', "help").text("prints this usage text")
      )
    }

    val config = ConfigurationLoader().loadConfig().get

    val comp = Option(System.getenv("COMP_LINE"))
      .zip(Option(System.getenv("COMP_POINT")).map(_.toInt))

    comp match {
      case Some(line -> point) => onCompleting(config, line, point)
      case None =>
        val cmd = OParser.parse(parser, args, Options(false, "", "")).get
        onGenerate(config, cmd)
    }
  }
}
