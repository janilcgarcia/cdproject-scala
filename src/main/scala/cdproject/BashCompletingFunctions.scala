package cdproject

object BashCompletingFunctions {
  def completingWord(line: String, captures: Seq[String], point: Int): String = {
    def go(
      rest: Seq[String],
      lineIndex: Int,
      currentIndex: Int
    ): String = rest match {
      case "" +: Nil => ""
      case s +: ss =>
        if (lineIndex >= point) {
          s.substring(0, currentIndex)
        } else if (s.isEmpty) {
          go(ss, lineIndex + 1, 0)
        } else if (currentIndex >= s.size) {
          go(ss, lineIndex, 0)
        } else if (line(lineIndex) == s(currentIndex)) {
          go(
            rest,
            lineIndex + 1,
            currentIndex + 1
          )
        } else {
          go(rest, lineIndex + 1, currentIndex)
        }
    }

    go(captures, 0, 0)
  }
}
