package cdproject

import scala.util.parsing.combinator._
import scala.util.{Either, Left, Right}

class ShParsers extends RegexParsers {
  override def skipWhitespace: Boolean = false

  def ws: Parser[Char] = """\s""".r ^^ { _.head }
  def number: Parser[String] = """\d+""".r ^^ { _.toString }

  def wordChar: Parser[Char] = """[\p{IsAlphabetic}\p{Digit}\-_:$/?@~,\.^/\*]|\\.""".r ^^ { s =>
    if (s.startsWith("\\"))
      s.charAt(1)
    else
      s.charAt(0)
  }

  def singleQuoteSequenceChar: Parser[Char] = """[^']""".r ^^ { _.head }
  def doubleQuoteSequenceChar: Parser[Char] = """\"|.""".r ^^ { s =>
    if (s.startsWith("\\"))
      s.apply(1)
    else
      s.apply(0)
  }

  def simpleWord: Parser[String] = rep1(wordChar).map { _.mkString }
  def doubleQuoteWord: Parser[String] = ("\"" ~> rep(doubleQuoteSequenceChar) <~ "\"") ^^ { _.mkString }
  def singleQuoteWord: Parser[String] = ("'" ~> rep(singleQuoteSequenceChar) <~ "'") ^^ { _.mkString }

  def word: Parser[String] = rep1(simpleWord | doubleQuoteWord | singleQuoteWord) ^^ {
    words => words.mkString
  }

  def command: Parser[Seq[String]] = word ~ opt(rep1(ws) ~ opt(command)) ^^ {
    case word ~ None => Seq(word)
    case word ~ Some(space ~ None) => Seq(word, "")
    case word ~ Some(space ~ Some(seq)) => word +: seq
  }
}

object ShCommandParser extends ShParsers {
  def split(line: String): Either[NoSuccess, Seq[String]] =
    parseAll(command, line) match {
      case Success(cmd, _) => Right(cmd)
      case v: NoSuccess => Left(v)
    }
}
