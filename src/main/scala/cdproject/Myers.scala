package cdproject

import scala.collection.immutable.HashMap
import scala.annotation.tailrec

object Myers {
  /** Cell within the DP matrix.
    * 
    * This is a bit-vetor cell, that means it holds a partial vector that
    * composes to form a DP matrix column.
    * 
    * @param pv Bit-vector containing positive values (+1)
    * @param mv Bit-vector containing negative values (-1)
    * @param score The so-far calculated score of the cell
    * @param mask A bit-mask used by the Myers algorithm
    */
  private final case class Cell(
    val pv: Long,
    val mv: Long,
    val score: Int,

    val mask: Long
  ) {
    /** Cell update step
      * 
      * Updates this cell based on the eq vector (calculated from the currently
      * evaluated text element.
      * 
      * @return The updated cell and the hout of this step
      */
    def step(eq: Long, hin: Int): (Cell, Int) = {
      val xv = eq | mv
      val eq1 = if (hin < 0) eq | 1 else eq

      val xh = (((eq1 & pv) + pv) ^ pv) | eq1

      val ph = mv | ~(xh | pv)
      val mh = pv & xh

      val hout =
        if ((ph & mask) != 0)
          1
        else if ((mh & mask) != 0)
          -1
        else
          0

      val ph1 = ph << 1 | (if (hin > 0) 1 else 0)
      val mh1 = mh << 1 | (if (hin < 0) 1 else 0)

      val pv1 = mh1 | ~(xv | ph1)
      val mv1 = ph1 & xv

      copy(pv = pv1, mv = mv1, score = score + hout) -> hout
    }
  }

  private object Cell {
    def apply(w: Int, size: Int): Cell =
      Cell(-1L >>> (w - size), 0L, 0, 1L << (size - 1))
  }

  /** Creates an eq map for any alphabet based on the pattern. The map
   *  contains alphabet latters as keys (which can be any hashable type in)
   *  this implementation and bit-vectors such that:
   *  Map(letter)(i) = (pattern(i) == letter)
   * 
   *  @param pn the pattern to extract the vectors from
   *  @return a Map of letters to bit-vector indicating active positions of
   *  the letter in the pattern
   */
  private def equalityVectors[A](pattern: Seq[A]): Map[A, Long] =
    pattern.foldLeft((1L, HashMap.empty[A, Long])) { case ((mask, map), p) =>
      (mask << 1, map.updatedWith(p)(_.map(_ | mask).orElse(Some(mask))))
    }._2.withDefault(_ => 0L)

  /** A update step for the entire column
    *
    * @param c The column
    * @param peqs The peq vectors
    * @param el The element being processed in this column
    * @return The updated column
    */
  private def stepColumn[A](c: Seq[Cell], peqs: Seq[Map[A, Long]], el: A): Seq[Cell] = {
    val eqs = peqs.map(_(el))

    def go(newCol: Seq[Cell], cells: Seq[(Cell, Long)], hin: Int): Seq[Cell] =
      cells match {
        case Seq() =>
          newCol.reverse

        case (cell, eq) +: rest =>
          val (cell1, hout) = cell.step(eq, hin)

          go(cell1 +: newCol, rest, hout)
      }

    go(Seq.empty, c.zip(eqs), 0)
  }

  /** Applies the generalized algorithm described at [https://doi.org/10.1145/316542.316550
    * "A fast bit-vector algorithm for approximate string matching based on dynamic programming"]
    * by Gene Meyers. It should take O(m + nm/w) to run, where m is the length of the pattern,
    * n is length of the text and w is the word size (64-bits for this implementation).
    * For patterns < 64 bits this algorithm runs in O(m + n) time.
    * 
    * @param pattern pattern being looked for
    * @param text text in which to search the pattern
    * @return a List of (score, position) pairs with the column score for each position
    */
  private def run[A](pattern: Seq[A], text: Seq[A]): Seq[(Int, Int)] = {
    val w = java.lang.Long.BYTES * 8
    val patterns = pattern.grouped(w).toIndexedSeq
    val peqs = patterns.map(equalityVectors)


    val column = patterns.map(_.size).map(Cell(w, _))

    text.scanLeft[Seq[Cell]](column) { (column, el) =>
      stepColumn(column, peqs, el)
    }.map(_.last.score).zipWithIndex
  }

  /** Calculate the distance between the pattern of the string as a function of
    * it's k-errors deltas. Long sequences affect more the score, both in case
    * of increasing or decreasing distance. The more the sequence alternate
    * the smaller is the distance.
    * 
    * @param m the length of the pattern
    * @param n The length of the text
    * @param deltas a sequence of delta * index
    * @return a distance metric
    */
  private def distance(m: Int, n: Int, deltas: Seq[Int]): Double = {
    def approx(acc: Double) =
      if (acc < 1.0)
        acc * acc
      else {
        val d = 2 - acc
        2 - d * d
      }

    val (startDelta, startAcc) = if (deltas.head == 0) (1, 1.5) else (-1, 0.5)

    val (score, acc, _, a) = deltas.drop(1).foldLeft((0.0, startAcc, startDelta, 0)) {
      case ((score, acc, last, n), delta) =>
        val newAcc = (last, delta) match {
          case 0 -> 1 | (-1) -> 1 => 1.1
          case 0 -> (-1) | 1 -> (-1) => 0.9

          case 1 -> 1 | (-1) -> (-1) => approx(approx(acc))

          case _ => approx(acc)
        }

        val newSeq = delta != 0 && last != delta
        val newScore = if (newSeq) score + acc else score

        (newScore, newAcc, delta, n + (if (newSeq) 1 else 0))
    }

    val adjust = m.toDouble / n

    adjust * (score + acc) / (2 * (a + 1))
  }

  /** Inverse of the distance function
    * 
    * @param m the length of the pattern
    * @param n the length of the text
    * @param deltas a sequence of delta * index
    * @return a similarity metric
    */
  private def similarity(m: Int, n: Int, deltas: Seq[Int]): Double =
    1 - distance(m, n, deltas)

  /** Convert a list of scores into a list of deltas
    *
    * @param rs a seq of score * index
    * @return a list of score deltas
    */
  private def scoresToDeltas(rs: Seq[(Int, Int)]) =
    rs.map(_._1).sliding(2).foldLeft(Seq.empty[Int]) {
      case (deltas, Seq(scoreA, scoreB)) =>
        (scoreB - scoreA) +: deltas
    }

  /** Calculate the score of a text pair.
   *  
   *  @param pattern the pattern being searched
   *  @param text The text
   *  @return A similarity score [0.0, 1.0], where 0.0 means completely distant
   *  and 1.0 means equal.
   */
  def score[A](pattern: Seq[A], text: Seq[A]): Double = {
    val rs = run(pattern, text)
    similarity(pattern.size, text.size, scoresToDeltas(rs))
  }

  /** Get the best matches for the pattern in the text list.
   * 
   *  @param pattern the pattern being searched
   *  @param text a sequence of texts for lookup
   *  @return a sequence of text * score ordered by similiarity, from high to
   *  low
   */
  def bestMatches[A, B <: Seq[A]](pattern: B, texts: Seq[B]): Seq[(B, Double)] = {
    val rss = texts.zip(texts.map(run(pattern, _)))

    val bestError = rss.flatMap { case (_, rs) => rs.map(_._1) }.min

    rss.filter { case (_, rs) => rs.exists { case (k, _) => k == bestError } }
      .map { case (text, rs) =>
        text -> similarity(pattern.length, text.length, scoresToDeltas(rs))
      }
      .sortBy(-_._2)
  }
}
