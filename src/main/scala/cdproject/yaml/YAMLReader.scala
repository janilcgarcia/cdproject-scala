package cdproject.yaml

import scala.util.Try
import scala.util.Failure
import scala.util.Success
import scala.collection.BuildFrom
import scala.collection.SeqOps
import scala.collection.Factory
import scala.collection.MapFactory
import scala.collection.IterableOnceOps
import scala.collection.MapOps

trait YAMLReader[T] {
  def read(o: YAMLValue): Try[T]
}

object YAMLReader {
  implicit val selfReader: YAMLReader[YAMLValue] = new YAMLReader[YAMLValue] {
    def read(o: YAMLValue): Try[YAMLValue] = Success(o)
  }

  implicit val anyReader: YAMLReader[Any] = new YAMLReader[Any] {
    def read(o: YAMLValue): Try[Any] = o match {
      case YString(s) => Success(s)
      case YInteger(i) => Success(i)
      case YDecimal(value) => Success(value)
      case YList(values) => Success(values.map(read(_).get))
      case YBoolean(value) => Success(value)
      case YObject(value) => Success(value.map{ case (k, v) => read(k).get -> read(v).get })
    }
  }

  implicit val stringReader: YAMLReader[String] = new YAMLReader[String] {
    def read(o: YAMLValue): Try[String] = o match {
      case YString(s) => Success(s)
      case _ => Failure(new IllegalArgumentException("Must be YString"))
    }
  }

  implicit val integerReader: YAMLReader[Int] = new YAMLReader[Int] {
    def read(o: YAMLValue): Try[Int] = o match {
      case YInteger(value) => Success(value)
      case _ => Failure(new IllegalArgumentException("Must be a YInteger"))
    }
  }

  implicit val doubleReader: YAMLReader[Double] = new YAMLReader[Double] {
    def read(o: YAMLValue): Try[Double] = o match {
      case YDecimal(value) => Success(value)
      case _ => Failure(new IllegalArgumentException("Must be a YDecimal"))
    }
  }

  implicit val booleanReader: YAMLReader[Boolean] = new YAMLReader[Boolean] {
    def read(o: YAMLValue): Try[Boolean] = o match {
      case YBoolean(value) => Success(value)
      case _ => Failure(new IllegalArgumentException("Must be a YBoolean"))
    }
  }

  implicit def collectionReader[A, CC[_] <: SeqOps[A, Seq, Seq[A]]](
    implicit factory: Factory[A, CC[A]],
    reader: YAMLReader[A]
  ): YAMLReader[CC[A]] = new YAMLReader[CC[A]] {
    def read(o: YAMLValue): Try[CC[A]] = o match {
      case YList(values) =>
        values.map(reader.read _).foldLeft(Try(factory.newBuilder)) { (acc, v) =>
          acc.flatMap(builder => v.map(builder.addOne _))
        }.map(_.result())

      case _ => Failure(new IllegalArgumentException("Must be a YList"))
    }
  }

  implicit def mapReader[A, B, CC[A, B] <: MapOps[A, B, Map, Map[A, B]]](
    implicit factory: Factory[(A, B), CC[A, B]],
    keyReader: YAMLReader[A],
    valueReader: YAMLReader[B]
  ): YAMLReader[CC[A, B]] = new YAMLReader[CC[A, B]] {
    def read(o: YAMLValue): Try[CC[A, B]] = o match {
      case YObject(m) =>
        m.map { case (key, value) =>
          for {
            k <- keyReader.read(key)
            v <- valueReader.read(value)
          } yield (k, v)
        }.foldLeft(Try(factory.newBuilder)) { (acc, pair) =>
          for {
            builder <- acc
            p <- pair
          } yield builder.addOne(p)
        }.map(_.result())

      case _ => Failure(new IllegalArgumentException("Must be a YObject"))
    }
  }
}