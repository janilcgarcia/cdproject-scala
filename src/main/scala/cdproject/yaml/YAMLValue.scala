package cdproject.yaml

import org.snakeyaml.engine.v2.api.Load
import org.snakeyaml.engine.v2.api.LoadSettings
import scala.util.Using
import scala.util.Try
import java.io.FileReader
import java.io.File
import java.nio.file.Path

sealed trait YAMLValue {
  def read[T : YAMLReader] = implicitly[YAMLReader[T]].read(this)
}

final case class YString(val value: String) extends YAMLValue {
  override def toString() = s""""$value""""
}

final case class YList(val value: List[YAMLValue]) extends YAMLValue {
  override def toString() = "Y" + value.toString
}

final case class YObject(val value: Map[YAMLValue, YAMLValue]) extends YAMLValue {
  override def toString() = {
    val contents = value.iterator.map { case (key, value) =>
      s"$key -> $value"
    }.scanLeft(("", 0)) { case ((_, count), s) =>
      (s, count + 2 + s.size)
    }.drop(1).takeWhile { case (s, count) =>
      count - 2 < 200 
    }.map(_._1).mkString(", ")

    s"YObject($contents)"
  }
}

final case class YInteger(val value: Int) extends YAMLValue {
  override def toString() = value.toString
}

final case class YDecimal(val value: Double) extends YAMLValue {
  override def toString() = value.toString
}

final case class YBoolean(val value: Boolean) extends YAMLValue {
  override def toString() = value.toString
}

object YAMLValue {
  import scala.jdk.CollectionConverters._

  private lazy val defaultLoad = new Load(LoadSettings.builder().build())

  private def convert(o: AnyRef): YAMLValue = o match {
    case m: java.util.Map[_, _] =>
      val immutable = m.asInstanceOf[java.util.Map[AnyRef, AnyRef]].asScala.toMap

      YObject(immutable.map { case (key, value) =>
        convert(key) -> convert(value)
      })

    case l: java.util.List[_] =>
      val immutable = l.asInstanceOf[java.util.List[AnyRef]].asScala.toList

      YList(immutable.map(convert _))

    case s: String => YString(s)
    case i: Integer => YInteger(i)
    case d: java.lang.Double => YDecimal(d)

    case b: java.lang.Boolean => YBoolean(b)

    case _ =>
      throw new IllegalArgumentException(s"Invalid argument type: ${o.getClass().getName}")
  }

  def parseFromString(s: String): Try[YAMLValue] = Try {
    convert(defaultLoad.loadFromString(s))
  }

  def parseFromFile(file: File): Try[YAMLValue] =
    Using(new FileReader(file)) { reader =>
      convert(defaultLoad.loadFromReader(reader))
    }

  def parseFromFile(name: String): Try[YAMLValue] =
    parseFromFile(new File(name))

  def parseFromFile(path: Path): Try[YAMLValue] =
    parseFromFile(path.toFile())
}