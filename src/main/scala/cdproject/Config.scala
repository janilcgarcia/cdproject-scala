package cdproject

import java.nio.file.{Path, Paths}
import yaml._
import scala.util.Try
import java.io.File

case class Config(
  val projectFilePatterns: Seq[ProjectFilePattern] =
    Seq(ProjectFilePattern.SimplePattern(".git")),
  val projectDirectories: Map[String, File] = Map.empty,
  val defaultRoot: Option[String] = None,
  val threshold: Double = 0.6
) {
  def isValid: Boolean =
    !projectFilePatterns.isEmpty && !projectDirectories.isEmpty

  def makeValid: Option[Config] =
    if (!isValid)
      None
    else
      Some(Config(
        projectFilePatterns,
        projectDirectories,
        defaultRoot.orElse(Some(projectDirectories.keys.head))
      ))

    def getDirectory(query: String): File = if (query.isEmpty()) {
        defaultRoot.map(projectDirectories(_)).get
    } else {
        projectDirectories(query)
    }
}

object Config {
  def loadFromFile(name: String): Try[Config] =
    YAMLValue.parseFromFile(name).flatMap(_.read[Config])

  implicit val fileReader: YAMLReader[File] = new YAMLReader[File] {
    private val home = Option(System.getenv("HOME"))
      .orElse(Option(System.getProperty("user.home")))

    def read(o: YAMLValue): Try[File] = for {
      ps <- o.read[String]

      ps1 = if (ps.startsWith("~")) ps.replaceFirst("~", home.get) else ps
    } yield FileExtensions.file(ps1)
  }

  implicit val configReader: YAMLReader[Config] = new YAMLReader[Config] {
    def read(o: YAMLValue): Try[Config] = for {
      m <- o.read[Map[String, YAMLValue]]

      patterns <- Try(m("project-file-patterns")).flatMap(_.read[Vector[ProjectFilePattern]])
      dirs <- Try(m("project-directories")).flatMap(_.read[Map[String, File]])
      root <- Try(m("default-root")).flatMap(_.read[String])
      threshold <- m.get("threshould").map(_.read[Double]).getOrElse(Try(0.6))
    } yield Config(patterns, dirs, Some(root), threshold)
  }
}
