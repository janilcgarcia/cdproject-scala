package cdproject

object CodePoint {
  implicit class CodePointSeqString(s: String) {
    def codePointSeq: Seq[Int] = {
      import scala.jdk.StreamConverters._

      s.codePoints().toScala(Seq)
    }
  }
}
