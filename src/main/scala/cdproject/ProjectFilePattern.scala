package cdproject

import yaml._
import java.nio.file.Path
import scala.util.matching.Regex
import scala.util.Try

sealed trait ProjectFilePattern {
  def matches(file: String): Boolean

  def matchesAny(basePath: Path): Boolean =
    basePath.toFile().list().exists(matches _)
}

object ProjectFilePattern {
  final case class RegexPattern(val regex: Regex) extends ProjectFilePattern {
    def matches(file: String): Boolean =
      regex.matches(file)
  }

  final case class SimplePattern(val name: String) extends ProjectFilePattern {
    def matches(file: String): Boolean =
      name == file
  }

  final case class ExtensionPattern(val extension: Seq[String]) extends ProjectFilePattern {
    def matches(file: String): Boolean = extension.exists(file.endsWith(_))
  }

  implicit val projectFilePatternReader: YAMLReader[ProjectFilePattern] =
    new YAMLReader[ProjectFilePattern] {
      def read(o: YAMLValue): Try[ProjectFilePattern] = for {
        s <- o.read[String]
      } yield if (s.startsWith("re/"))
        RegexPattern(s.substring(3).r)
      else if (s.startsWith("ext/"))
        ExtensionPattern(s.substring(4).split(',').toIndexedSeq.map("." + _))
      else
        SimplePattern(s)
    }
}

