package cdproject

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class ShCommandParserSepc extends AnyFlatSpec with Matchers {
  "The ShCommandParser" should "split a shell string" in {
    ShCommandParser.split("cmd -o --opt arg1 arg2 path/") shouldBe Right(Seq("cmd", "-o", "--opt", "arg1", "arg2", "path/"))
    ShCommandParser.split("is this valid?") shouldBe Right(Seq("is", "this", "valid?"))
    ShCommandParser.split("cmd ar* /there") shouldBe Right(Seq("cmd", "ar*", "/there"))
    ShCommandParser.split("cdp ") shouldBe Right(Seq("cdp", ""))
  }

  it should "not accept invalid caracters" in {
    ShCommandParser.split("cmd 022 &").isLeft shouldBe true
    ShCommandParser.split("cmd > file").isLeft shouldBe true
    ShCommandParser.split("cmd =10").isLeft shouldBe true
    ShCommandParser.split("#this is a comment").isLeft shouldBe true
    ShCommandParser.split("! nope").isLeft shouldBe true
    ShCommandParser.split("cmd $MY_VAR $(ls -l)").isLeft shouldBe true
    ShCommandParser.split("[ -n $A 10 ]").isLeft shouldBe true
    ShCommandParser.split("cmd ab{c,d}").isLeft shouldBe true
    ShCommandParser.split("cmd abc; cmd abcd").isLeft shouldBe true
    ShCommandParser.split("cmd > out").isLeft shouldBe true
    ShCommandParser.split("cmd `other cmd` continues").isLeft shouldBe true
    ShCommandParser.split("cmd | no | piping").isLeft shouldBe true
  }

  it should "accept invalid characters if escaped" in {
    val examples = "!#><=[]{}|`()"
    def command(c: Char): (Seq[String], String) = {
      val seq = Seq(
        "cmd",
        s"quoted line $c",
        s"escaped\\${c}char"
      )


      seq.map(_.replace("\\", "")) -> seq.map(a => if ("\\s".r.findFirstIn(a).isDefined)
        s"'$a'"
      else
        a
      ).mkString(" ")
    }

    examples.map(command).foreach { case (seq, s) =>
      ShCommandParser.split(s) shouldBe Right(seq)
    }
  }
}
