package cdproject

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class BashCompletingFunctionsSpec extends AnyFlatSpec with Matchers {
  import BashCompletingFunctions._

  "The completingWord" should "find an empty when completing after space" in {
    completingWord("cdp ", Seq("cdp", ""), 4) shouldBe ""
  }

  it should "find the last argument if completing in the last position of the argument" in {
    completingWord("cdp em", Seq("cdp", "em"), 6) shouldBe "em"
  }

  it should "find the a subtring of the last argument if completing in the mid position of it" in {
    completingWord("cdp emacs", Seq("cdp", "emacs"), 6) shouldBe "em"
  }

  it should "find an empty string in the middle" in {
    completingWord("cdp '' -p", Seq("cdp", "", "-p"), 6) shouldBe ""
  }
}
